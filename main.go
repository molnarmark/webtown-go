package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"math"
	"net/http"
	"net/url"
	"strconv"
)

// Az elérhető termékek
var stock = []map[string]interface{}{
	{
		"name":        "Téliszalámi",
		"price":       2000,
		"megapack":    false,
		"browsername": "teliszalami",
	},
	{
		"name":        "Gumikacsa",
		"price":       3000,
		"megapack":    false,
		"browsername": "gumikacsa",
	},
	{
		"name":        "Megapack Uborka",
		"price":       2800,
		"megapack":    true,
		"browsername": "megapack-uborka",
	},
	{
		"name":        "Megapack Gesztenye",
		"price":       1000,
		"megapack":    true,
		"browsername": "megapack-gesztenye",
	},
}

// container a kosárnak
var cart []*Item

// Egy item
type Item struct {
	name     string
	price    int
	quantity int
	megapack bool
}

func NewItem(name string, price int, quantity int, megapack bool) *Item {
	return &Item{
		name:     name,
		price:    price,
		quantity: quantity,
		megapack: megapack,
	}
}

// Kiszámolja, összeadja, illetve véglegesíti a discountokat.
func applyDiscounts(cart []*Item) (int, string) {
	pay2Get3TotalDiscount := 0
	megapackTotalDiscount := 0

	for _, cartItem := range cart {
		if cartItem.megapack {
			megapackTotalDiscount += cartItem.CheckMegapackDiscount()
		} else {
			pay2Get3TotalDiscount += cartItem.CheckPay2Get3Discount()
		}
	}

	if pay2Get3TotalDiscount > megapackTotalDiscount {
		return pay2Get3TotalDiscount, "Pay 2 Get 3"
	} else {
		return megapackTotalDiscount, "Megapack"
	}
}

// Kiszámolja a megapack discountot
func (i *Item) CheckMegapackDiscount() int {
	return int(math.Floor(float64((i.quantity / 12) * 6000)))

}

// Kiszámolja a 2őt fizet 3at kap discountot
func (i *Item) CheckPay2Get3Discount() int {
	return int(math.Floor(float64((i.quantity / 3) * i.price)))
}

// Beolvas egy fájlt a neve alapján
func getSitePart(filename string) []byte {
	contents, err := ioutil.ReadFile(filename)

	if err != nil {
		return make([]byte, 0)
	}
	return contents
}

// Itemekké alakítja a POST requesttel küldött form értékeket, és rádobja a cartra
func parseForm(values url.Values) {
	for key, value := range values {
		intValue, _ := strconv.ParseInt(value[0], 10, 64)
		item := findItemFromMap(key)
		cart = append(cart, (NewItem(
			item["name"].(string),
			item["price"].(int),
			int(intValue),
			item["megapack"].(bool),
		)))
	}
}

// Visszakeresi az itemet a name property alapján (browsername) a stockból
func findItemFromMap(browsername string) map[string]interface{} {
	var foundItem map[string]interface{}

	for _, item := range stock {
		if browsername == item["browsername"] {
			foundItem = item
		}
	}

	return foundItem
}

func fileToTemplate(filename string) (*template.Template, bytes.Buffer) {
	page := getSitePart(filename)
	t, _ := template.New(filename).Parse(string(page))
	var writeBuffer bytes.Buffer
	return t, writeBuffer
}

func main() {
	indexTemplate, writeBuffer := fileToTemplate("form.html")
	indexTemplate.Execute(&writeBuffer, stock)

	thankYouTemplate, thankYouBuffer := fileToTemplate("thankyou.html")

	// / route kezelése
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Ha sima GET, visszaküldjük neki form html fájlt
		if r.Method == "GET" {
			w.Write(writeBuffer.Bytes())

			// Ha POST, feldolgozzuk az adatokat, kiszámoljuk a discountot és lerendereljük az új templatet, majd visszaküldjük
		} else if r.Method == "POST" {
			r.ParseForm()
			parseForm(r.Form)
			rate, name := applyDiscounts(cart)
			thankYouTemplate.Execute(&thankYouBuffer, map[string]interface{}{"discountRate": rate, "chosenDiscount": name})
			w.Write(thankYouBuffer.Bytes())
			cart = cart[:0]
			thankYouBuffer.Reset()
		}
	})

	// HTTP Szerver a 8080as porton
	fmt.Println("Tesztapp fut => http://localhost:8080")
	http.ListenAndServe(":8080", nil)
}
